const responseListener = require('./responseListener');
const logger = require('./utils/logger');

const scraperObject = {
    async scraper(browser, params){

        //create a new page/tab in browser
        let page = await browser.newPage();
        //await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36')

        let data = {};
        data.body = "Nothing happend";
        data.response = {};
        data.response.status = 501;

        //passing cookies to the page
        if(params.cookies) {
            //console.log(JSON.parse(params.cookies));
            cookies = JSON.parse(params.cookies)
            for (cooka in cookies) {
                await page.setCookie({"name": cooka, "value":cookies[cooka], "url":params.url});
            }
        }

        //passing headers to the page
        //let headers = params.headers;
        //console.log(params.headers);
        if(params.headers) { await page.setExtraHTTPHeaders(JSON.parse(params.headers)); }
        if(params.agent) { await page.setUserAgent(params.agent); }

        try {

            if (params.url.includes("parcelsapp.com")) {
                const response = await responseListener(page, params.url, 'https://parcelsapp.com/api/v2/parcels');
                data.body = await response.json();
                data.response.status = await response.status();
                if ('error' in data.body) {
                    logger().error(`{filename: "pageScraper.js", url: "${params.url}", status: ${data.response.status}, message: "${data.body.error}"}`, "inside try 1");
                } else {
                    logger().info(`{filename: "pageScraper.js", url: "${params.url}", status: ${data.response.status}, message: ${JSON.stringify(data.body || {})}`, "inside try 2");
                }
                await page.evaluate(_ => window.stop());
            }

            //navigate page/tab to specified url
            //console.log(`Navigating to ${params.url}...`);
            //get full page html with evaluting JS on it
            if (!params.url.includes("parcelsapp.com")) {
                const page_goto = await page.goto(params.url, { waitUntil: 'load' });
                data.body = await page.evaluate(() => document.querySelector('*').outerHTML);
                data.response.status = await page_goto.status();
            }
        } catch (e) {
            data.response.status = 500;
            data.body = e;
            console.error('Goto browser error:', e);
            logger().error(`{filename: "pageScraper.js", url: "${params.url}", message: ${typeof e === 'object' ? JSON.stringify(e) : `"${e}"`}}`, "inside catch");
        } finally {
            await page.close();
        }

        //return page html to express webserver
        return data;
    }
}

module.exports = scraperObject;
