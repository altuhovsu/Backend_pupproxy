const { createLogger, format, transports } = require('winston');

const logger = () => createLogger({
    format: format.combine(
        format.timestamp(),
        format.errors({ stack: true }),
        format.printf(info => {
            return `${info.timestamp} [${info.level.toUpperCase()}]: ${(info.stack || info.message)}, [META]: ${info[Symbol.for('splat')]}`;
        }),
    ),
    transports: [
        new transports.File({ filename: `./logs/${new Date().toLocaleDateString()}-[ERRORS].log`, level: 'error' }),
        new transports.File({ filename: `./logs/${new Date().toLocaleDateString()}-[COMBINED].log` })
    ]
});

module.exports = logger;

