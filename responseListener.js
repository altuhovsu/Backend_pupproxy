const logger = require('./utils/logger');

const timeoutMs = 30_000;

const listener = (page, urlToGo, urlToListen) => new Promise(async (resolve, reject) => {
    let timeout = setTimeout(() => {
        logger().error(`{filename: "responseListener.js", url: "${urlToGo}", message: "Timeout 30s"}`);
        reject('[responseListener] Timeout');
    }, timeoutMs);

    page.on('response', async (response) => {
        if (response.url() === urlToListen) {
            clearTimeout(timeout);
            resolve(response);
        }
    });

    await page.goto(urlToGo);
});

module.exports = listener;
